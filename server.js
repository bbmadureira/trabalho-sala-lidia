const express = require('express');
const bodyParser = require('body-parser');
const app = express();

//BANCO DE DADOS
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/crud', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Conexão Sucedida!! ')
});

app.use(bodyParser.urlencoded({ extended: true}));

app.listen(3000, function(){
    console.log('server running on port 3000')
});

app.get('/', (req, res) => {
    res.render('index.ejs');
});

app.set('view engine', 'ejs');

function findAll(callback) {
  db.collection("data").find({}).toArray(callback); // Mostrar
}

//INSERIR
//salvando no banco de dados e monstrando na pag. show
app.post('/show', (req, res) => {
    findAll((e, docs) => {
    if (e) {
      return console.log(e)
    }
    let count = 0
    for (let i = 0; i < docs.length; i++) {

      if (docs[i].name === req.body.name) {
        count = 1
      }
    }
    if (count > 0) {
      console.log("Valor Duplicado!");
    } else {
      db.collection("data").insertOne(req.body, (err, r) => {
        if (err) {
          return console.log(err)
        }
        console.log('Salvo no banco de Dados')
      })
    }
    res.redirect('/show');
    })
})

app.get('/', (req, res) => {
    let cursor = db.collection('data').find();
})

app.get('/show', (req, res) => {
    db.collection('data').find().toArray((err, results) => {
        if (err) return console.log(err)
        console.log(results);  
        res.render('show.ejs', { data: results })
    })
})

//armazenando o ID em var
app.route('/edit/:id')
.get((req, res) => {
    var id = req.params.id
    let ObjectId = require("mongodb").ObjectId;
    db.collection('data').find(new ObjectId(id)).toArray((err, result) => {
        if (err) return res.send(err);
        console.log(result)
        res.render('edit.ejs', { data: result });
    })
})
//Update
.post((req, res) => {
    var id = req.params.id
    var name = req.body.name
    var surname = req.body.surname
    let ObjectId = require("mongodb").ObjectId;
    db.collection('data').updateOne({ _id: new ObjectId(id) }, {
        $set: {
            name: name,
            surname: surname
        }
    }, (err, result) => {
        if (err) return res.send(err)
            res.redirect('/show')
        console.log('Atualizado no Banco de Dados')
    })
})
//DELETANDO DO BANCO DE DADOS
app.route('/delete/:id')
.get((req, res) => {
    var id = req.params.id
    let ObjectId = require("mongodb").ObjectId;
    db.collection('data').deleteOne({ _id: new ObjectId(id) }, (err, result) => {
        if (err) return res.send(500, err)
            console.log('Deletado do Banco de Dados!')
        res.redirect('/show')
    })
})
